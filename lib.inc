section .text
%define DEC 10
%define nulascii 48
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax
    mov rax, 60
    xor rdi, rdi
    syscall
    ret
    

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
    	mov rsi, [rdi]
    	and rsi, 0xff
    	test rsi, rsi
    	jz .exit
    	inc rax
    	inc rdi
    	jmp .loop
    .exit: 
    	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    mov rdx, 1
    .loop:
    	mov r10b, [rsi]
    	test r10b, r10b
    	jz .exit
    	syscall
    	inc rsi
    	jmp .loop
    .exit:
    	ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r10, DEC
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0
    mov r11, 1
    .loop:
        xor rdx, rdx
    	div r10
    	dec rsp
    	add rdx, nulascii
    	mov [rsp], dl
    	inc r11
    	test rax, rax
    	jz .print_number
    	jmp .loop
    .print_number:
    	mov rdi, rsp
    	push r11
    	call print_string
    	pop r11
    	add rsp, r11
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi
    js .neg
    .pos:
    	call print_uint
    	jmp .exit
    .neg:
    	mov r10, rdi
    	mov rdi, 45
    	call print_char
    	mov rdi, r10
    	neg rdi
    	call print_uint
    	
    	
    .exit:
    	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
    	mov r10b, [rdi]
    	mov r11b, [rsi]
    	cmp r10b, r11b
    	jnz .nequals
    	test r10b, r10b
    	jz .equals
    	inc rdi
    	inc rsi
    	jmp .loop
    .equals:
    	mov rax, 1
    	jmp .exit
    .nequals:
    	mov rax, 0
    	jmp .exit
    .exit:
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    mov rdi, 0
    dec rsp
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jz .exit
    mov al, [rsp]
    
    .exit:
    	inc rsp
    	ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
  xor rax, rax
  mov r10, rsi
  test r10, r10 
  jz .error
  xor r11, r11
  dec r10

  .looptab:
    push rdi
    push r11
    call read_char
    pop r11
    pop rdi
    cmp rax, 0x20
    jz .looptab
    cmp rax, 0x9
    jz .looptab
    cmp rax, 0xA
    jz .looptab
    test r10, r10
    jz .error
    test rax, rax
    jz .sucs
    mov [rdi+r11], al
    dec r10
    inc r11
    jmp .loop

  .loop:
    push rdi
    push r11
    call read_char
    pop r11
    pop rdi
    cmp rax, 0x20
    jz .sucs
    cmp rax, 0x9
    jz .sucs
    cmp rax, 0xA
    jz .sucs
    test rax, rax
    jz .sucs
    test r10, r10
    jz .error
    mov [rdi+r11], al
    inc r11
    dec r10
    jmp .loop

  .sucs:
    mov byte [rdi + r11], 0
    mov rax, rdi
    mov rdx, r11
    jmp .exit


  .error:
    xor rax, rax
    jmp .exit

  .exit:
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov rsi, 0
    mov r10, 10
    xor rdx, rdx
    .loop:
    	xor r11, r11
     	mov r11b, [rdi+rsi]
     	cmp r11b, 48
     	jb .exit
     	cmp r11b, 57
     	ja .exit
     	sub r11b, 48
     	mul r10
     	add rax, r11
     	inc rsi
     	jmp .loop
     	
    .exit:
    	mov rdx, rsi
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov rsi, 0
    mov r10, 10
    xor rdx, rdx
    xor r11, r11
    mov r11b, [rdi+rsi]
    cmp r11b, 45
    jz .neg
    jmp .pos
    .pos:
    	call parse_uint
    	jmp .exit
    	
    .neg:
    	inc rdi
    	call parse_uint
    	imul rax, -1
    	inc rdx
    	jmp .exit
    	
    .exit:
    	ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rdx, rax
    jl .error
    mov rdx, rax
    .loop:
    	mov al, [rdi]
    	mov byte[rsi], al
    	test rax, rax
    	jz .sucs
    	inc rdi
    	inc rsi
    	jmp .loop
    .sucs:
    	mov rax, rdx
    	jmp .exit
    .error:
    	xor rax, rax
    	jmp .exit
    .exit:
    	ret
